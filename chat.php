<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Онлайн чат</title>

    <?php include 'components/head.php'; ?>

</head>
<body>
    <?php include 'components/sidebar.php' ?>
    <div class="app">
    <?php include 'components/header.php' ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Онлайн чат</h5>
                    <div class="divider"></div>
                </div>
            </div>

            <div class="body-block pa-2">
                <div id="chat">
                    <div class="user-lists">
                        <div class="search">
                            <div class="input-field">
                                <i class="material-icons prefix">search</i>
                                <input id="search-chat-user" type="text" class="validate">
                                <label for="search-chat-user">Поиск</label>
                            </div>
                        </div>
                    </div>
                    <div class="mail-body">
                        <div class="messages">
                            <div class="message left">
                                <img src="/public/images/profile.png" alt="">
                                <div class="body">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis voluptates commodi ut tempora cum?</p>
                                    <span>12 минут назад</span>
                                </div>
                            </div>
                            <div class="message right">
                                <img src="/public/images/profile.png" alt="">
                                <div class="body">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis voluptates commodi ut tempora cum?</p>
                                    <span>12 минут назад</span>
                                </div>
                            </div>
                        </div>
                        <div class="mail-control">
                            <div class="input-field">
                                <textarea id="description" class="materialize-textarea"></textarea>
                                <label for="description">Сообщение</label>
                            </div>
                            <div class="buttons">
                                <a href="#!" class="open-smile tooltipped" data-position="top" data-tooltip="Смайлики"><i class="material-icons">sentiment_satisfied</i></a>
                                <a href="#!" class="send tooltipped" data-position="top" data-tooltip="Отправить"><i class="material-icons">send</i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="fixed-action-btn">
            <a href="#!" class="btn-floating btn-large green waves-effect pulse tooltipped" data-position="left" data-tooltip="Добавить">
                <i class="large material-icons">add</i>
            </a>
        </div>
    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>