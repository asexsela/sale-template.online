<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Комментарии</title>

    <?php include 'components/head.php'; ?>

</head>
<body>
    <?php include 'components/sidebar.php' ?>
    <div class="app">
    <?php include 'components/header.php' ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Комментарии</h5>
                    <div class="divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col s6">

                </div>
                <div class="col s12 l3 offset-l3">
                    <div class="input-field">
                        <input id="search" type="text" class="validate">
                        <label for="search">Поиск</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <table class="responsive-table highlight comments">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Активность</th>
                                <th>Пользователь</th>
                                <th>Контент</th>
                                <th>Комментарий</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>1</td>
                                <td><span class="yes">Да</span></td>
                                <td><a href="">Иванов Иван</a></td>
                                <td><span class="yes">Товар</span><a href="">Брюки</a></td>
                                <td style="width: 60%;">
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis velit, repellat adipisci numquam minima non pariatur mollitia id, officiis vitae voluptates suscipit maiores asperiores! Eligendi cumque adipisci ullam aliquid, voluptatum cupiditate. Fugiat expedita laboriosam quos voluptate voluptatem fugit repellat, repellendus facilis quidem reiciendis, magni harum, rem ipsa ab praesentium at?
                                    <div class="control">
                                        <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                        <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><span class="no">Нет</span></td>
                                <td><a href="">Иванов Иван</a></td>
                                <td><span class="no">Статья</span><a href="">Брюки</a></td>
                                <td style="width: 60%;">
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis velit, repellat adipisci numquam minima non pariatur mollitia id, officiis vitae voluptates suscipit maiores asperiores!
                                    <div class="control">
                                        <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                        <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="col s12">
                    <ul class="pagination right">
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                        <li class="active"><a href="#!">1</a></li>
                        <li class="waves-effect waves-green"><a href="#!">2</a></li>
                        <li class="waves-effect waves-green"><a href="#!">3</a></li>
                        <li class="waves-effect waves-green"><a href="#!">4</a></li>
                        <li class="waves-effect waves-green"><a href="#!">5</a></li>
                        <li class="waves-effect waves-green"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                    </ul>
                </div>
            </div>

            
        </div>

    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>