<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Категория 1</title>

    <?php include 'components/head.php'; ?>

</head>
<body>
    <?php include 'components/sidebar.php' ?>
    <div class="app">
    <?php include 'components/header.php' ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Категория 1</h5>
                    <div class="divider"></div>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m6 l12 xl6">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="category-title" type="text" class="validate">
                            <label for="category-title">Наименование</label>
                        </div>
                        <div class="input-field">
                            <input id="category-slug" type="text" class="validate">
                            <label for="category-slug">URL</label>
                        </div>
                        <div class="input-field select-category">
                            <select>
                                <option value="" disabled selected>Выберете категорию</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                            </select>
                            <label>Категория</label>
                        </div>
                        <div class="file-field input-field">
                            <div class="btn file">
                                <span>Выбрать файл</span>
                                <input type="file" id="upload-image">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l12 xl6">  
                    <div class="body-block pa-2">  
                        <p>Предпросмотр изображения</p>
                        <img class="materialboxed" id="image" width="100%" src="">
                    </div>
                </div>
            </div>



            <div class="fixed-action-btn">
                <a class="btn-floating btn-large blue pulse">
                    <i class="large material-icons">more_vert</i>
                </a>
                <ul>
                    <li><a href="/category" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                    <li><a onclick="M.toast({html: 'Успешно'})" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
                </ul>
            </div>
        </div>

    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>