<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Вход</title>

    <?php include 'components/head.php'; ?>

</head>
<body>
    <div id="admin-login">
        <div class="preview">
            <img class="animated fadeIn slow" src="public/images/dev-logo.png" alt="">
            <h1 class="animated fadeInUp slow">Лаборатория новых технологий</h1>
            <p class="animated fadeInUp slow">Добро пожаловать в админ панель сайта sale-template.ru</p>
        </div>
        <div class="login">
            <form id="login" class="admin-form">
                <h3>Вход</h3>
                <div class="row">
                    <div class="input-field col s12">
                    <input id="email" type="email" class="validate">
                    <label for="email">Почта</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input id="password" type="password" class="validate">
                    <label for="password">Пароль</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s6">
                        <a class="forgot" href="/forgot">Забыли пароль?</a>
                    </div>
                    <div class="col s6">
                        <a href="/dashboard" class="waves-effect waves-light btn right">Вход</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>