<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Товар 1</title>

    <?php include 'components/head.php'; ?>
    

</head>
<body>
    <?php include 'components/sidebar.php' ?>
    <div class="app">
    <?php include 'components/header.php' ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Товар 1</h5>
                    <div class="divider"></div>
                </div>
            </div>

            <div class="row">
                    <div class="col s12 m8 l12 xl8">
                        <div class="body-block pa-2">
                            <div class="input-field">
                                <input id="product-title" type="text" class="validate">
                                <label for="product-title">Наименование</label>
                            </div>
                            <div class="input-field">
                                <textarea id="description" class="materialize-textarea"></textarea>
                                <label for="description">Небольшое описание</label>
                            </div>
                            <div class="input-field">
                                <div id="editor"></div>
                            </div>
                            
                        </div>
                        <div class="body-block pa-2">
                            <div class="input-field">
                                <textarea id="meta" class="materialize-textarea"></textarea>
                                <label for="meta">Описание (meta)</label>
                            </div>

                            <div class="input-field">
                                <textarea id="desc" class="materialize-textarea"></textarea>
                                <label for="desc">Ключевые слова (keywords)</label>
                            </div>

                            <div class="input-field">
                                <input id="seo-title" type="text" class="validate">
                                <label for="seo-title">SEO заголовок</label>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 l12 xl4">
                        <div class="body-block pa-2">
                            <div class="input-field">
                                <input id="product-slug" type="text" class="validate">
                                <label for="product-slug">URL</label>
                            </div>
                            <div class="input-field">
                                <input id="product-price" type="text" class="validate">
                                <label for="product-price">Цена</label>
                            </div>
                            <div class="input-field select-product">
                                <select>
                                    <option value="" selected>Выберете категорию</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                </select>
                                <label>Категория</label>
                            </div>
                            <div class="input-field">
                                <div class="chips">
                                    <input class="custom-class">
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col s12 xl4">
                                    <div class="input-field">
                                        <p>Публикация</p>
                                        <div class="switch">
                                            <label>
                                                Нет
                                                <input type="checkbox" checked>
                                                <span class="lever"></span>
                                                Да
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 xl4">
                                    <div class="input-field">
                                        <p>Новинка</p>
                                        <div class="switch">
                                            <label>
                                                Нет
                                                <input type="checkbox">
                                                <span class="lever"></span>
                                                Да
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 xl4">
                                    <div class="input-field">
                                        <p>Рекомендованный</p>
                                        <div class="switch">
                                            <label>
                                                Нет
                                                <input type="checkbox">
                                                <span class="lever"></span>
                                                Да
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="input-field">
                                <p>Дата публикации</p>
                                <input type="text" class="datepicker" placeholder="Выберете дату публикации">
                                <span class="helper-text">Если оставить пустым, то опубликуется сразу</span>
                            </div> 
                        </div>
                        <div class="body-block pa-2">
                            <p>Основное изображение</p>
                            <div class="file-field input-field">
                                <div class="btn file">
                                    <span>Выбрать изображение</span>
                                    <input type="file" id="upload-image">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                            <p>Предпросмотр изображения</p>
                            <img class="materialboxed" id="image" width="100%" src="">
                        </div>

                        <div class="body-block pa-2">
                            <p>Галлерея</p>
                            <div class="file-field input-field">
                                <div class="btn file">
                                    <span>Выбрать изображения</span>
                                    <input type="file" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="">
                                </div>
                            </div>
                            
                        </div>

                    </div>

            </div>



            <div class="fixed-action-btn">
                <a class="btn-floating btn-large blue pulse">
                    <i class="large material-icons">more_vert</i>
                </a>
                <ul>
                    <li><a class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                    <li><a class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
                </ul>
            </div>
        </div>

    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>