<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Категории</title>

    <?php include 'components/head.php'; ?>

</head>
<body>
    <?php include 'components/sidebar.php' ?>
    <div class="app">
    <?php include 'components/header.php' ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Категории</h5>
                    <div class="divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col s6">

                </div>
                <div class="col s12 l3 offset-l3">
                    <div class="input-field">
                        <input id="search" type="text" class="validate">
                        <label for="search">Поиск</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <table class="responsive-table highlight">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Наименование</th>
                                <th>Обновление</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Брюки</td>
                                <td>
                                    12 минут назад
                                    <div class="control">
                                        <a href="#!" class="edit tooltipped" data-position="top" data-tooltip="Редактировать"><i class="material-icons">edit</i></a>
                                        <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Верхняя одежда</td>
                                <td>
                                    22.07.2020
                                    <div class="control">
                                        <a href="#!" class="edit tooltipped" data-position="top" data-tooltip="Редактировать"><i class="material-icons">edit</i></a>
                                        <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Обувь</td>
                                <td>
                                    22.07.2020
                                    <div class="control">
                                        <a href="#!" class="edit tooltipped" data-position="top" data-tooltip="Редактировать"><i class="material-icons">edit</i></a>
                                        <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col s12">
                    <ul class="pagination right">
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                        <li class="active"><a href="#!">1</a></li>
                        <li class="waves-effect waves-green"><a href="#!">2</a></li>
                        <li class="waves-effect waves-green"><a href="#!">3</a></li>
                        <li class="waves-effect waves-green"><a href="#!">4</a></li>
                        <li class="waves-effect waves-green"><a href="#!">5</a></li>
                        <li class="waves-effect waves-green"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                    </ul>
                </div>
            </div>

            
        </div>
        <div class="fixed-action-btn">
            <a href="/category-add" class="btn-floating btn-large green waves-effect pulse tooltipped" data-position="left" data-tooltip="Добавить">
                <i class="large material-icons">add</i>
            </a>
        </div>
    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>