$(function() {

    setTimeout(function() {
        $('#admin-login').addClass('show');
    }, 2000)


    $('.sidenav').sidenav();
    $('.collapsible').collapsible();
    $('.fixed-action-btn').floatingActionButton();
    $('select').formSelect();
    $('.materialboxed').materialbox();
    $('.chips').chips({
        placeholder: 'Теги',
        secondaryPlaceholder: '+Тег',
    });
    $('.datepicker').datepicker({
        firstDay: 1,
        format: 'dd mmm yyyy',
        i18n: {
            cancel: 'Отмена',
            clear: 'Очистить',
            done: 'Сохранить',
            months: [
                'Январь',
                'Февраль',
                'Март',
                'Апрель',
                'Май',
                'Июнь',
                'Июль',
                'Август',
                'Сентябь',
                'Октябрь',
                'Ноябрь',
                'Декабрь'
            ],
            monthsShort: [
                'Янв',
                'Фев',
                'Мар',
                'Апр',
                'Май',
                'Июн',
                'Июл',
                'Авг',
                'Сен',
                'Окт',
                'Ноб',
                'Дек'
            ],
            weekdays: [
                'Воскресенье',
                'Понедельник',
                'Вторник',
                'Среда',
                'Четверг',
                'Пятница',
                'Суббота'
            ],
            weekdaysShort: [
                'Вск',
                'Пн',
                'Вт',
                'Ср',
                'Чт',
                'Пт',
                'Сб'
            ],
            weekdaysAbbrev: ['В','П','В','С','Ч','П','С']
        }
    });
    $('.tooltipped').tooltip();

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#image').attr('src', e.target.result);
            };
    
            reader.readAsDataURL(input.files[0]);

            $('#image').css('display', 'block');

        }
    }
    
    $("#upload-image").change(function(){
        readURL(this);
    });

    let src = $('.materialboxed')

    if(src.attr('src') === '') {
        src.css('display', 'none');
    }

   
})

SmoothScroll({ stepSize: 20 })

ClassicEditor
    .create(document.querySelector('#editor'), {
        language: 'ru',
    })
    // .then(editor => {
    //         // console.log(editor);
    // })
    // .catch(error => {
    //         // console.log(error);
    // });

ClassicEditor
    .create(document.querySelector('#editor1'), {
        language: 'ru',
    })

VK.Widgets.Group("vk_groups", {
    mode: 4, 
    wide: 1, 
    no_cover: 1, 
    height: "400",
    color1: '#08554b',
    color2: 'FFFFFF', 
    color3: 'C0C0C0'
}, 143365133);


