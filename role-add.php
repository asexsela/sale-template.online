<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Новая роль</title>

    <?php include 'components/head.php'; ?>

</head>
<body>
    <?php include 'components/sidebar.php' ?>
    <div class="app">
    <?php include 'components/header.php' ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Новая роль</h5>
                    <div class="divider"></div>
                </div>
            </div>

            <div class="row">
                <form class="col s12">

                    <div class="body-block pa-2">

                        <div class="input-field">
                            <input id="role-title" type="text" class="validate">
                            <label for="role-title">Наименование</label>
                        </div>

                        <div class="input-field">
                            <input id="role-title" type="text" class="validate">
                            <label for="role-title">Отображаемое наименование</label>
                        </div>
                        
                    </div>
                    
                </form>
            </div>



            <div class="fixed-action-btn">
                <a class="btn-floating btn-large blue pulse">
                    <i class="large material-icons">more_vert</i>
                </a>
                <ul>
                    <li><a href="/roles" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                    <li><a onclick="M.toast({html: 'Успешно'})" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
                </ul>
            </div>
        </div>

    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>