<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Иванов Иван</title>

    <?php include 'components/head.php'; ?>
    

</head>
<body>
    <?php include 'components/sidebar.php' ?>
    <div class="app">
    <?php include 'components/header.php' ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Иванов Иван</h5>
                    <div class="divider"></div>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m8 l12 xl8">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="user-name" type="text" class="validate">
                            <label for="user-name">ФИО</label>
                        </div>
                        <div class="row">
                            <div class="col s6">
                                <div class="input-field">
                                    <input id="user-email" type="email" class="validate">
                                    <label for="user-email">Email</label>
                                </div>
                            </div>
                            <div class="col s6">
                                <div class="input-field">
                                    <input id="user-phone" type="text" class="validate">
                                    <label for="user-phone">Телефон</label>
                                </div>
                            </div>
                        </div>
                        <div class="input-field">
                            <textarea id="description" class="materialize-textarea"></textarea>
                            <label for="description">Адрес доставки</label>
                        </div>                         
                    </div>
                    <div class="body-block pa-2">
                        <p>Активные заказы</p>

                        <table class="highlight">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Дата</th>
                                    <th>Товар</th>
                                    <th>Цена</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>11.12.2020</td>
                                    <td>Eclair</td>
                                    <td>$0.87</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>11.12.2020</td>
                                    <td>Jellybean</td>
                                    <td>$3.76</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>11.12.2020</td>
                                    <td>Lollipop</td>
                                    <td>$7.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="body-block pa-2">
                        <p>История заказов</p>

                        <table class="highlight">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Дата</th>
                                    <th>Товар</th>
                                    <th>Цена</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>11.12.2020</td>
                                    <td>Eclair</td>
                                    <td>$0.87</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>11.12.2020</td>
                                    <td>Jellybean</td>
                                    <td>$3.76</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>11.12.2020</td>
                                    <td>Lollipop</td>
                                    <td>$7.00</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>11.12.2020</td>
                                    <td>Eclair</td>
                                    <td>$0.87</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>11.12.2020</td>
                                    <td>Jellybean</td>
                                    <td>$3.76</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>11.12.2020</td>
                                    <td>Lollipop</td>
                                    <td>$7.00</td>
                                </tr>
                                
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                            <li class="active"><a href="#!">1</a></li>
                            <li class="waves-effect waves-green"><a href="#!">2</a></li>
                            <li class="waves-effect waves-green"><a href="#!">3</a></li>
                            <li class="waves-effect waves-green"><a href="#!">4</a></li>
                            <li class="waves-effect waves-green"><a href="#!">5</a></li>
                            <li class="waves-effect waves-green"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col s12 m4 l12 xl4">
                    <div class="body-block pa-2">
                        <div class="file-field input-field">
                            <div class="btn file">
                                <span>Выбрать файл</span>
                                <input type="file" id="upload-image">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                        <p>Логотип</p>
                        <img class="materialboxed" id="image" width="100%" src="">
                    </div>
                    <div class="body-block pa-2">
                        <form>
                            <div class="input-field">
                                <input id="user-passwd" type="password" class="validate">
                                <label for="user-passwd">Пароль</label>
                            </div>
                            <div class="input-field">
                                <input id="user-passwd-confirm" type="password" class="validate">
                                <label for="user-passwd-confirm">Подтвердите пароль</label>
                            </div>
                            <a class=" waves-effect waves-light btn file">Изменить</a>
                        </form>
                    </div>
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="user-sale" type="text" class="validate">
                            <label for="user-sale">Скидка</label>
                        </div>
                        <div class="input-field select-category">
                            <select>
                                <option value="" selected>Выберете роль</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                            </select>
                            <label>Роль</label>
                        </div>
                        <div class="input-field">
                            <p>Активный</p>
                            <div class="switch">
                                <label>
                                    Нет
                                    <input type="checkbox" checked>
                                    <span class="lever"></span>
                                    Да
                                </label>
                            </div>
                        </div>
                        <div class="input-field">
                            <p>День рождения</p>
                            <input type="text" class="datepicker" placeholder="Выберете дату рождения">
                        </div> 
                    </div>
                    <div class="body-block pa-2">
                        <p>Общая сумма заказов</p>
                        <p class="all-price">32 000 руб</p>
                    </div>

                </div>

            </div>



            <div class="fixed-action-btn">
                <a class="btn-floating btn-large blue pulse">
                    <i class="large material-icons">more_vert</i>
                </a>
                <ul>
                    <li><a class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                    <li><a class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
                </ul>
            </div>
        </div>

    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>