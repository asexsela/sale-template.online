<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper">
            <div class="row">
                <form class="col s3 hide-on-small-only">
                        <div class="input-field">
                            <i class="material-icons prefix">search</i>
                            <input id="icon_prefix" type="text" class="validate">
                            <label for="icon_prefix">Поиск</label>
                        </div>
                </form>
                <ul class="right">
                    <li><a href="/user/1" class="tooltipped" data-position="bottom" data-tooltip="Профиль"><span class="online"></span>Иван Иванов</a></li>
                    <li><a href="/messages" class="tooltipped" data-position="bottom" data-tooltip="Сообщения"><i class="material-icons">email</i><span class="new badge"  data-badge-caption="">4</span></a></li>
                    <li><a href="/comments" class="tooltipped" data-position="bottom" data-tooltip="Комментарии"><i class="material-icons">comment</i><span class="new badge"  data-badge-caption="">12</span></a></li>
                    <li><a href="/" class="tooltipped" data-position="bottom" data-tooltip="Выход"><i class="material-icons">exit_to_app</i></a></li>
                    <li class="hide-on-large-only"><a href="/" data-target="slide-out" class="sidenav-trigger" class="tooltipped" data-position="bottom" data-tooltip="Меню"><i class="material-icons">menu</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<a href="#name"></a>