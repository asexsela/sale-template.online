
<ul id="slide-out" class="sidenav sidenav-fixed">
    <!-- <li><div class="user-view">
        <div class="background">
        <img src="/public/images/bg.jpg">
        </div>
        <a href="#user"><img class="circle" src="/public/images/profile.png"></a>
        <a href="#name"><span class="white-text name">Иван Иванов</span></a>
        <a href="#email"><span class="white-text email">sexsela@gmail.com</span></a>
    </div></li> -->
    <li><a href="/dashboard" class="waves-effect waves-green"><i class="material-icons">dashboard</i>Приборная панель</a></li>
    <li><a href="/category" class="waves-effect waves-green"><i class="material-icons">category</i>Категории</a></li>
    <li><a href="/news" class="waves-effect waves-green"><i class="material-icons">post_add</i>Статьи</a></li>
    <li><a href="/products" class="waves-effect waves-green"><i class="material-icons">folder</i>Товары</a></li>
    <!-- <li>
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header waves-effect" >Страницы<i class="material-icons">pages</i></a>
                <div class="collapsible-body">
                    <ul>
                    <li><a href="#!">Главная</a></li>
                    <li><a href="#!">О нас</a></li>
                    <li><a href="#!">Контакты</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li> -->
    <li><a href="/users" class="waves-effect waves-green"><i class="material-icons">supervised_user_circle</i>Пользователи</a></li>
    <li><a href="/chat" class="waves-effect waves-green"><i class="material-icons">email</i>Чат</a></li>
    <li><a href="/comments" class="waves-effect waves-green"><i class="material-icons">comment</i>Комментарии</a></li>
    <li>
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">Настройки<i class="material-icons">settings_applications</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="/settings">Основные</a></li>
                        <li><a href="/roles">Роли</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>

    <li><div class="divider"></div></li>
    <li><a target="blank" href="//laboratory.company"><i class="material-icons">child_care</i>Сайт разработчика</a></li>  
    <li><a class="subheader">Группа ВК</a></li>
    <div id="vk_groups"></div>

</ul>