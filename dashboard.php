<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Dashboard</title>

    <?php include 'components/head.php'; ?>

</head>
<body>
    <?php include 'components/sidebar.php'; ?>
    <div class="app">
        <!-- <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a> -->
        <?php include 'components/header.php'; ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Приборная панель</h5>
                    <div class="divider"></div>
                </div>
            </div>
            <div class="row blocks-info">
                <div class="col s6 xl3">

                    <div class="info">
                        <p>Пользователи</p>
                        <span>12</span>
                    </div>
                    
                </div>
                <div class="col s6 xl3">

                    <div class="info">
                        <p>Товары</p>
                        <span>12</span>
                    </div>

                </div>

                <div class="col s6 xl3">

                    <div class="info">
                        <p>Сумма заказов</p>
                        <span>12</span>
                    </div>

                </div>
                <div class="col s6 xl3">

                    <div class="info">
                        <p>Статьи</p>
                        <span>12</span>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 xl6">
                    <canvas id="productData"></canvas>
                </div>
                <div class="col s12 m6 xl6">
                    <canvas id="allData"></canvas>
                </div>
            </div>
            <div class="row">
                <div class="col s12 xl6">
                    <h5>Новые комментарии</h5>
                    <div class="divider"></div>
                    <div class="disabled-comment-box">
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cumque consequatur earum sit.</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a>
                                <a href="#!">Товар 1,</a>
                                <span>5 минут назад</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi itaque, commodi vel explicabo alias tempora ex repellendus. Et accusantium facilis voluptatibus quibusdam nesciunt ea amet quaerat ipsam odio in. Molestias unde iste praesentium, illo, voluptatem nemo asperiores reprehenderit obcaecati quasi adipisci quae veniam fuga incidunt et? Eos id ipsam alias!</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a> 
                                <a href="#!">Товар 1,</a> 
                                <span>20 минут назад</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cumque consequatur earum sit.</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a> 
                                <a href="#!">Товар 1,</a> 
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cumque consequatur earum sit.</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a>
                                <a href="#!">Товар 1,</a>
                                <span>5 минут назад</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi itaque, commodi vel explicabo alias tempora ex repellendus. Et accusantium facilis voluptatibus quibusdam nesciunt ea amet quaerat ipsam odio in. Molestias unde iste praesentium, illo, voluptatem nemo asperiores reprehenderit obcaecati quasi adipisci quae veniam fuga incidunt et? Eos id ipsam alias!</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a> 
                                <a href="#!">Товар 1,</a> 
                                <span>20 минут назад</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cumque consequatur earum sit.</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a> 
                                <a href="#!">Товар 1,</a> 
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cumque consequatur earum sit.</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a>
                                <a href="#!">Товар 1,</a>
                                <span>5 минут назад</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi itaque, commodi vel explicabo alias tempora ex repellendus. Et accusantium facilis voluptatibus quibusdam nesciunt ea amet quaerat ipsam odio in. Molestias unde iste praesentium, illo, voluptatem nemo asperiores reprehenderit obcaecati quasi adipisci quae veniam fuga incidunt et? Eos id ipsam alias!</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a> 
                                <a href="#!">Товар 1,</a> 
                                <span>20 минут назад</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cumque consequatur earum sit.</p>
                            <div class="info-comment">
                                <a href="#!">Иванов Иван,</a> 
                                <a href="#!">Товар 1,</a> 
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="check tooltipped" data-position="top" data-tooltip="Одобрить"><i class="material-icons">check</i></a>
                                <a href="#!" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col s12 xl6">
                    <h5>Новые пользователи</h5>
                    <div class="divider"></div>
                    <div class="news-users-box">
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Иванов Иван Иванович</p>
                            <div class="info-comment">
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Иванов Иван Иванович</p>
                            <div class="info-comment">
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Иванов Иван Иванович</p>
                            <div class="info-comment">
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Иванов Иван Иванович</p>
                            <div class="info-comment">
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Иванов Иван Иванович</p>
                            <div class="info-comment">
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Иванов Иван Иванович</p>
                            <div class="info-comment">
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/public/images/profile.png" alt="">
                            <p>Иванов Иван Иванович</p>
                            <div class="info-comment">
                                <span>22.05.2020 г.</span>
                            </div>
                            <div class="control">
                                <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large green pulse">
                <i class="large material-icons">add</i>
            </a>
            <ul>
                <li><a href="/category-add" class="btn-floating red tooltipped" data-position="left" data-tooltip="Добавить категорию"><i class="material-icons">category</i></a></li>
                <li><a href="/news-add" class="btn-floating yellow darken-1 tooltipped" data-position="left" data-tooltip="Добавить статью"><i class="material-icons">post_add</i></a></li>
                <li><a href="/user-add" class="btn-floating teal tooltipped" data-position="left" data-tooltip="Добавить пользователя"><i class="material-icons" >supervised_user_circle</i></a></li>
            </ul>
        </div>
    </div>

    <?php include 'components/scripts.php'; ?>

    <script>
        $(function () {

            var ctx = document.getElementById('productData').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line',

                // The data for our dataset
                data: {
                    labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    datasets: [{
                        label: 'Заказы',
                        backgroundColor: '#03A9F4',
                        borderColor: '#0c97d6',
                        data: [0, 10, 5, 2, 20, 30, 45, 30, 20, 10, 50, 35]
                    }]
                },

                // Configuration options go here
                options: {}
            });

            var ctx = document.getElementById('allData').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'pie',

                // The data for our dataset
                data: {
                    labels: ['Товары', 'Статьи', 'Категории', 'Пользователи'],
                    datasets: [{
                        label: 'Заказы',
                        backgroundColor: ['#4caf50', '#E91E63', '#9C27B0', '#2196F3'],
                        borderColor: '#fff',
                        data: [20, 30, 45, 50]
                    }]
                },

                // Configuration options go here
                options: {}
            });
        })
    </script>

</body>
</html>