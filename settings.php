<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ панель || Основные настройки</title>

    <?php include 'components/head.php'; ?>

</head>
<body>
    <?php include 'components/sidebar.php' ?>
    <div class="app">
    <?php include 'components/header.php' ?>
        <div class="had-container">
            <div class="row">
                <div class="col s12">
                    <h5>Основные настройки</h5>
                    <div class="divider"></div>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m8 l12 xl8">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="company-name" type="text" class="validate">
                            <label for="company-name">Название компании</label>
                        </div>
                        <div class="input-field select-category">
                            <div class="input-field">
                                <textarea id="address" class="materialize-textarea"></textarea>
                                <label for="address">Адрес</label>
                            </div> 
                        </div>

                        <div class="row">
                            <div class="col s12 l5">
                                <div class="input-field">
                                    <input id="lat" type="text" class="validate">
                                    <label for="lat">Широта</label>
                                </div>
                            </div>
                            <div class="col s12 l5">
                                <div class="input-field">
                                    <input id="lng" type="text" class="validate">
                                    <label for="lng">Долгота</label>
                                </div>
                            </div>
                            <div class="col s12 l2">
                                <div class="input-field">
                                    <input id="zoom" type="text" class="validate">
                                    <label for="zoom">Увеличение</label>
                                </div>
                            </div>
                        </div>
                        <div data-lat="" data-lng="" data-zoom="" id="map"></div>
                    </div>
                </div>
                <div class="col s12 m4 l12 xl4">  
                    <div class="body-block pa-2">  
                        <div class="input-field">
                            <input id="phone" type="text" class="validate">
                            <label for="phone">Телефон</label>
                        </div>
                        <div class="input-field">
                            <input id="site-mail" type="text" class="validate">
                            <label for="site-mail">Почта для сайта</label>
                        </div>
                        <div class="mails">
                            <div class="input-field">
                                <input id="mail" type="text" class="validate">
                                <label for="mail">Почта для писем</label>
                            </div>

                        </div>
                        <a class=" waves-effect waves-light btn file">+</a>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col s12 xl6">
                    <p>Политика конфиденциальности</p>
                    <div class="input-field">
                        <div id="editor"></div>
                    </div>
                </div>
                <div class="col s12 xl6">
                    <p>Обработка персональных данных</p>
                    <div class="input-field">
                        <div id="editor1"></div>
                    </div>
                </div>
            </div>



            <div class="fixed-action-btn">
                <a class="btn-floating btn-large blue pulse">
                    <i class="large material-icons">more_vert</i>
                </a>
                <ul>
                    <li><a href="/category" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                    <li><a onclick="M.toast({html: 'Успешно'})" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
                </ul>
            </div>
        </div>

    </div>

    <?php include 'components/scripts.php'; ?>

</body>
</html>